package ru.psu.movs.trrp.socketmq.server;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class Utils {
    private static final String exchangePrefix = UUID.randomUUID().toString();

    private static String encodeResourceUrl(String resource) {
        String encodedResourceUrl;
        encodedResourceUrl = URLEncoder.encode(resource, StandardCharsets.UTF_8);
        return encodedResourceUrl;
    }

    public static String buildExchangeName(String resource) {
        return exchangePrefix + "" + encodeResourceUrl(resource);
    }
}
