package ru.psu.movs.trrp.socketmq.api;

public class ListResourcesRequest implements Request {
    private static final long serialVersionUID = -5184106530759081859L;

    @Override
    public RequestType getRequestType() {
        return RequestType.ListResources;
    }
}
