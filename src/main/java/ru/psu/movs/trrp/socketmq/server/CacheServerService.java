package ru.psu.movs.trrp.socketmq.server;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import ru.psu.movs.trrp.socketmq.AppConfig;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.*;

public class CacheServerService {
    private final Channel mqChannel;
    private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
    private final ExecutorService rssCacheAgentsThreadPool = Executors.newCachedThreadPool();
    private final Set<String> trackedResources = ConcurrentHashMap.newKeySet();
    private final Set<String> availableResources;

    public CacheServerService() throws IOException, TimeoutException {
        AppConfig appConfig = AppConfig.load();
        AppConfig.MessageQueueServer messageQueueServer = appConfig.messageQueueServer;
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(messageQueueServer.host);
        factory.setPort(messageQueueServer.port);
        factory.setUsername(messageQueueServer.username);
        factory.setPassword(messageQueueServer.password);
        mqChannel = factory.newConnection().createChannel();
        this.availableResources = Set.of(
                "http://www.kremlin.ru/events/all/feed",
                "http://government.ru/all/rss/",
                appConfig.consoleRssServer.address
        );
        scheduleRssCacheUpdateAgentsExecution();
    }

    public Set<String> listResources() {
        return availableResources;
    }

    public void subscribe(String resourceUrl, String queueName) throws IOException {
        if (!availableResources.contains(resourceUrl))
            return;
        trackedResources.add(resourceUrl);
        String exchangeName = Utils.buildExchangeName(resourceUrl);
        mqChannel.exchangeDeclare(exchangeName, "fanout", false, true, false, null);
        mqChannel.queueBind(queueName, exchangeName, "");
        System.out.printf("[%s] new subscriber: %s%n", resourceUrl, queueName);
    }

    private void scheduleRssCacheUpdateAgentsExecution() {
        Runnable loadResources = () -> trackedResources.forEach(resource ->
                rssCacheAgentsThreadPool.submit(new CacheUpdateAgent(resource, mqChannel))
        );
        scheduledExecutor.scheduleAtFixedRate(loadResources, 10, 20, TimeUnit.SECONDS);
    }
}
