package ru.psu.movs.trrp.socketmq.server;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndFeedImpl;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedOutput;
import ru.psu.movs.trrp.socketmq.AppConfig;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.http.HTTPBinding;
import java.io.StringReader;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CopyOnWriteArrayList;

@WebServiceProvider
@ServiceMode()
public class ConsoleRssServer implements Provider<Source> {
    // http://stackoverflow.com/a/24103800

    private static final List<SyndEntry> newsEntries = new CopyOnWriteArrayList<>();
    private static final SyndFeed feed = buildFeed();

    public static void main(String[] args) {
        AppConfig appConfig = AppConfig.load();
        Endpoint.create(HTTPBinding.HTTP_BINDING, new ConsoleRssServer()).publish(appConfig.consoleRssServer.address);
        System.out.println("[ConsoleRssServer] Running at " + appConfig.consoleRssServer.address);
        try (Scanner stdinScanner = new Scanner(System.in)) {
            while (stdinScanner.hasNextLine()) {
                String input = stdinScanner.nextLine().trim();
                if (!input.equals("")) {
                    SyndEntry entry = new SyndEntryImpl();
                    entry.setTitle(input);
                    entry.setUri(feed.getLink() + '/' + newsEntries.size());
                    newsEntries.add(0, entry);
                }
            }
        }
    }

    private static SyndFeed buildFeed() {
        SyndFeed feed = new SyndFeedImpl();
        feed.setFeedType("rss_2.0");
        feed.setTitle("TRRP RSS");
        feed.setLink("http://trrp.movs.psu.ru");
        feed.setDescription("Some trrp server");
        return feed;
    }

    public Source invoke(Source request) {
        feed.setEntries(newsEntries);
        SyndFeedOutput output = new SyndFeedOutput();
        try {
            return new StreamSource(new StringReader(output.outputString(feed)));
        } catch (FeedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
