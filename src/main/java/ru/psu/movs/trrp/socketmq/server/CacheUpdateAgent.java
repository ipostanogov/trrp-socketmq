package ru.psu.movs.trrp.socketmq.server;

import com.rabbitmq.client.Channel;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndEntryImpl;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import org.apache.commons.lang3.SerializationUtils;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class CacheUpdateAgent implements Runnable {

    private final static ConcurrentHashMap<String, String> lastUris = new ConcurrentHashMap<>();
    private final static ConcurrentHashMap<String, Boolean> runningAgents = new ConcurrentHashMap<>();
    private final String resource;
    private final Channel mqChannel;

    public CacheUpdateAgent(String resource, Channel mqChannel) {
        this.resource = resource;
        this.mqChannel = mqChannel;
    }

    @Override
    public void run() {
        if (runningAgents.putIfAbsent(resource, true) != null)
            return;
        try {
            // https://stackoverflow.com/a/5366647
            URL url = new URL(resource);
            InputStream is = url.openConnection().getInputStream();
            InputSource source = new InputSource(is);
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(source);
            String nullableLastEntryUri = lastUris.get(resource);
            List<SyndEntry> entries = feed.getEntries();
            io.vavr.collection.List<SyndEntry> newEntriesReversed = io.vavr.collection.List.ofAll(entries).takeWhile(
                    x -> !Objects.equals(x.getUri(), nullableLastEntryUri)
            ).reverse();
            newEntriesReversed.forEach(entry -> {
                        System.out.printf("[%s] new entry: %s%n", resource, entry.getUri());
                        try {
                            mqChannel.basicPublish(
                                    Utils.buildExchangeName(resource),
                                    "",
                                    null,
                                    SerializationUtils.serialize((SyndEntryImpl) entry));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
            if (!entries.isEmpty()) {
                lastUris.put(resource, entries.get(0).getUri());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            runningAgents.remove(resource);
        }
    }
}
